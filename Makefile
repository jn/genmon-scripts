PREFIX?=/usr/local
BINDIR?=$(PREFIX)/bin
DATADIR?=$(PREFIX)/share
icontheme:=$(DATADIR)/icons/hicolor

.PHONY: none
none:
	@echo Specify a target:  install, uninstall

.PHONY: install
install:
	@install -vDpm 0755 -t $(DESTDIR)$(BINDIR) $(wildcard scripts/*)
	@install -vDpm 0644 -t $(DESTDIR)$(icontheme)/16x16/status icons/oneko-16.png
	@install -vDpm 0644 -t $(DESTDIR)$(icontheme)/scalable/status icons/decaf-*.svg

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(BINDIR)/decaf
	rm -f $(DESTDIR)$(BINDIR)/genmon-cpu-temp
	rm -f $(DESTDIR)$(BINDIR)/genmon-gamemode
	rm -f $(DESTDIR)$(BINDIR)/genmon-mem-free
	rm -f $(DESTDIR)$(BINDIR)/genmon-network-status
	rm -f $(DESTDIR)$(BINDIR)/genmon-oneko
	rm -f $(DESTDIR)$(BINDIR)/genmon-time-copy
	rm -f $(DESTDIR)$(icontheme)/16x16/status/oneko-16.png
	rm -f $(DESTDIR)$(icontheme)/scalable/status/decaf-*.svg
