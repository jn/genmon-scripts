# genmon-scripts

Scripts for [xfce4-genmon-plugin](https://docs.xfce.org/panel-plugins/xfce4-genmon-plugin/start).

- <img alt="" src="icons/decaf-on.svg" height="16" width="16"> **[decaf](scripts/decaf)** — control for Xfce presentation mode (alternative to Caffeine and similar programs) [[screenshot](screenshot-decaf.png)]
- <img alt="" src="icons/oneko-16.png" height="16" width="16"> [genmon-oneko](scripts/genmon-oneko) — control for [Oneko](http://www.daidouji.com/oneko/) desktop toy[^fn-neko] [[screenshot](screenshot-genmon-oneko.png)]
- [genmon-cpu-temp](scripts/genmon-cpu-temp) — display CPU temperature
- [genmon-mem-free](scripts/genmon-mem-free) — display free memory
- [genmon-time-copy](scripts/genmon-time-copy) — display time and copy ISO datetime to clipboard
- [genmon-network-status](scripts/genmon-network-status) — display network status and copy external IP

## Requirements

- xfce4-genmon-plugin >=4.1.0 for `<icon>` support

## Installing

A simple Makefile is provided. The default installation is in `/usr/local`:

```shell
$ sudo make install
```

Set `PREFIX` to install somewhere else:

```shell
$ PREFIX=~/.local make install
```

### Manual installation

Place the script(s) in your path (e.g. `~/.local/bin`), and the icon(s) [somewhere in the icon theme spec](https://specifications.freedesktop.org/icon-theme-spec/0.10/ar01s03.html) (e.g. `~/.local/share/icons`).

## Usage

Add the genmon panel items as you like.

For decaf, please refer to the script for additional instructions.

## Uninstalling

Use `make uninstall` in the same way that you installed.

## License

Unless otherwise noted, the software in this repository is released under the [Zero Clause BSD License](https://landley.net/toybox/license.html) (<abbr title="Software Package Data Exchange">SPDX</abbr>: [0BSD]).

genmon-mem-free is released under the MIT License ([MIT]).

### Artwork

decaf-on.svg and decaf-off.svg are released under [Creative Commons Zero 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) ([CC0-1.0]).

oneko-16.png is based on the Neko artwork, which is public domain to the best of my knowledge[^fn-neko-license].


[^fn-neko]: See [Neko (software) - Wikipedia][wp-neko] and [猫に歴史あり (Cats have a history)](https://web.archive.org/web/19990922013535/http://hp.vector.co.jp/authors/VA004959/oneko/nekohist.html) for a brief history of Neko/xneko/oneko

[^fn-neko-license]: See [Debian's copyright file for _oneko_ (oneko-sakura)](https://metadata.ftp-master.debian.org/changelogs/main/o/oneko/oneko_1.2.sakura.6-15_copyright). The _xneko_ README dated 1990-07-11 (from [Slackware _xgames_ package](https://packages.slackware.com/?r=slackware64-current&p=xgames-0.3-x86_64-8.txz)) contains the phrase "public domain", and the [_oneko_ Fedora package states the license as such since at least 2006](https://src.fedoraproject.org/rpms/oneko/c/c62d3c7fa63767444654e766a46c937a847d3553?branch=fc6#d2h-994719).

[wp-neko]: https://en.wikipedia.org/wiki/Neko_(software)
[0BSD]: https://spdx.org/licenses/0BSD.html
[MIT]: https://spdx.org/licenses/MIT.html
[CC0-1.0]: https://spdx.org/licenses/CC0-1.0.html
