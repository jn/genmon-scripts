#!/usr/bin/bash

# SPDX-License-Identifier: 0BSD

# Copyright (C) 2022 by Justin Koh <j@ustink.org>
#
# Permission to use, copy, modify, and/or distribute this software
# for any purpose with or without fee is hereby granted.

#----------------------------------------------------------------------
# genmon script to display CPU temperature
# Avg temp is shown in the panel, individual temps in tooltip
#
# Requires: lm_sensors && (ripgrep || GNU grep)
#----------------------------------------------------------------------

# Check if sensors installed
if ! builtin hash sensors &>/dev/null; then
	cat <<-EOF
		<txt>Can't find sensors</txt>
	EOF
	exit
fi

readonly sensor='coretemp-isa-0000'

# Will contain multiline string with 1 temp per line
# 7 temps, first one is avg, the rest are from CPU 0 to 6
# Truncated to 1 decimal place
declare temps
if builtin hash rg; then
	# rg's $1 is regex match, not shell arg
	# shellcheck disable=SC2016
	temps="$(sensors -u "${sensor}" | rg --color=never -o -r '$1' 'temp\d+_input:\s*(\d+\.\d)')"
else
	# grep with PCRE for discard (\K)
	temps="$(sensors -u "${sensor}" | grep -P --color=never -o -e 'temp.+_input: *\K[[:digit:]]+\.[[:digit:]]')"
fi

# Map to array w/o newlines
declare -a temps_arr
mapfile -t temps_arr <<<"${temps}"

# Output genmon XML
cat <<-EOF
	<txt>${temps_arr[0]}</txt>
	<tool><b>CPU temperature:</b>
	${temps_arr[*]:1:3}
	${temps_arr[*]:4}</tool>
EOF
